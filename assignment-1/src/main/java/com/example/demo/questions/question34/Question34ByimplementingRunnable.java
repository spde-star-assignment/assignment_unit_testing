package com.example.demo.questions.question34;

class One2 implements Runnable {
	public void run() {
		for (int i = 0; i < 50; i++) {
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				System.out.println(e);
			}
			System.out.println("Good Morning");
		}
	}
}

class Two2 implements Runnable {
	public void run() {
		for (int i = 0; i < 50; i++) {
			try {
				Thread.sleep(2000);
			} catch (InterruptedException e) {
				System.out.println(e);
			}
			System.out.println("Hello ");
		}
	}
}

class Three2 implements Runnable {
	public void run() {
		for (int i = 0; i < 50; i++) {
			try {
				Thread.sleep(3000);
			} catch (InterruptedException e) {
				System.out.println(e);
			}
			System.out.println("Welcome");
		}
	}
}

public class Question34ByimplementingRunnable {
	public static void main(String[] args) {
		One2 tt1 = new One2();
		Two2 tt2 = new Two2();
		Three2 tt3 = new Three2();

		Thread t1 = new Thread(tt1);
		Thread t2 = new Thread(tt2);
		Thread t3 = new Thread(tt3);

		t1.start();
		t2.start();
		t3.start();
	}
}
