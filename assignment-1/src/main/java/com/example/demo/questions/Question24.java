package com.example.demo.questions;

import com.mysql.cj.exceptions.WrongArgumentException;

//Write unit tests for a program that should print squares of natural numbers (1,2….10) the square 
//should not go beyond 100, & array size should not store more than 10 elements. Generate exception 
//if range overflow.

public class Question24 {

	public static int[] printSqure(int num) throws WrongArgumentException {
		int arr[] = new int[num];
		int a =1;
		if (num > 10) {
			throw new WrongArgumentException("the range should not be greater than 100");
		} 
			for (int i = 0; i <num; i++) {
				arr[i] = (int) Math.pow(a,2);
				a++;
			}
	
		return arr;
	}

	public static void main(String[] args) {

//		System.out.println(Arrays.toString(printSqure(10)));
		
	}
}
