package com.example.demo.questions;

//Write unit test for program which initialization earning of an employee. The program should calculate 
//the income tax to be paid by the employee as per the criteria given below:

//Slab rate			 IT rate
//Upto Rs. 50,000 		Nil
//Upto Rs. 60,000 		10% on additional amount
//Upto Rs. 1,50,000 	20% on additional amount
//Above Rs. 1,50,000 	30% on additional amount

public class Question9 {

	public  double getTax(double amount) {
	
		double TaxableAmmount = amount-50000;
		
		if (amount <=50000) {
			return 0;
		} else if(amount >50000 && amount <=60000){
		//10
			return TaxableAmmount*0.1;
		} else if(amount >60000 && amount <=150000){
			//20
			return TaxableAmmount*0.2;
		} 	else {
			return TaxableAmmount*0.3;
		} 		
	}

	public static void main(String[] args) {
//		Question9 question9 = new Question9();
//		System.out.println(getTax(100000));
		
	}
}
