package com.example.demo.questions;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

//Write unit tests for JDBC program to connect the MySql database & get the count of tables & views in 
//that database

public class Question14 {
	public  int getCountOfTablesOrViews(String str) {

		int a = 0;
		try {

			Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/imgdb", "root", "root");
			Statement st = con.createStatement();
			ResultSet rs = st
					.executeQuery("SELECT COUNT(*) FROM information_schema." + str + " WHERE table_schema = 'imgdb'");
			while (rs.next()) {
				a = rs.getInt(1);
			}
			con.close();
		} catch (Exception e) {
			System.out.println(e);
		}
		return a;
	}

	public static void main(String args[]) {

//		System.out.println("number of tables " + getCountOfTablesOrViews("tables"));
//		System.out.println("number of views " + getCountOfTablesOrViews("views"));
	}
}
