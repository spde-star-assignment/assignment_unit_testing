package com.example.demo.questions;

import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

//Write unit tests for Print the current date with time zone, Add 10Hours to the existing date and print

public class Question38 {

	public static String getDateWithTimeZone() {
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MM/dd/yyyy - HH:mm:ss z");
		ZonedDateTime zdt = ZonedDateTime.now();
		String formattedZdt1 = zdt.format(formatter);
		return formattedZdt1;
	}

	public static String AddTimeToExistingDate(int hr) {
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MM/dd/yyyy - HH:mm:ss z");
		ZonedDateTime zdt = ZonedDateTime.now();
		zdt = zdt.plusHours(hr);
		String formattedZdt2 = zdt.format(formatter);
		return formattedZdt2;
	}

	public static void main(String[] args) {

//		System.out.println(getDateWithTimeZone());
//System.out.println(AddTimeToExistingDate(24));
		
//		  DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MM/dd/yyyy - HH:mm:ss z");
//		  String date = AddTimeToExistingDate(24);
//
//		  //convert String to LocalDate
//		  LocalDate localDate = LocalDate.parse(date, formatter);
//		  
//        System.out.println(localDate);
	}
}
