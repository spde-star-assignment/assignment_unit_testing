package com.example.demo.questions.question17;

public @SuppressWarnings("serial")
class WrongTimeException extends RuntimeException {   //exception class

	public WrongTimeException(String msg) {
		super(msg);
	}
}

