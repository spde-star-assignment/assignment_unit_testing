package com.example.demo.questions;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

//Write unit tests for a program to write a common data to 
//multiple files using a single stream only?

//https://www.codeproject.com/Questions/1233353/Write-to-multiple-files-with-java

public class Question3 {

	public static void main(String[] args) throws IOException
	{
	  String[] fileNames = {"file1.html","file2.txt"};
	  BufferedWriter writer = null;
	  for(int i=0;i<fileNames.length;i++)
	  {
	    System.out.printf("%d\n", i);
	    writer = new BufferedWriter(new FileWriter(fileNames[i]));
	    writer.write("Java is object oriented");
	    writer.close(); //<- flush the BufferedWriter
	  }
	}
}
