package com.example.demo.questions.question23;

public class Worker extends Employee {

	private int workerId;
	private String name;
	private double salary;
	private double additionalPay;
	private String address;

	
	
	public int getWorkerId() {
		return workerId;
	}

	public String getName() {
		return name;
	}

	public double getSalary() {
		return salary;
	}

	public double getAdditionalPay() {
		return additionalPay;
	}

	public String getAddress() {
		return address;
	}

	public Worker(int workerId, String name, double salary, double additionalPay, String address) {
		super();
		this.workerId = workerId;
		this.name = name;
		this.salary = salary;
		this.additionalPay = additionalPay;
		this.address = address;
	}

	@Override
	public double salary() {
		return this.salary + this.additionalPay;
	}

	@Override
	public String display() {
		String str = "Worker [workerId=" + workerId + ", name=" + name + ", salary=" + salary + ", additionalPay="
				+ additionalPay + ", address=" + address + "]";
		System.out.println(str);
		return str;
	}

}
