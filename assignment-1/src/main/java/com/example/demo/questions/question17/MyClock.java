package com.example.demo.questions.question17;

import com.mysql.cj.exceptions.WrongArgumentException;

public class MyClock { // ------------------------------------------------------- // clock class
	private int hour;
	private int minutes;
	private int seconds;
	private String status;

	public MyClock() {
		super();
	}

	
//	@Override
//	public String toString() {
//		return "time " + hour + " : " + minutes + " : " + +seconds + " " + status;
//	}

	public MyClock(int hour, int minutes, int seconds) {
		super();
		try {
			if (validateHour(hour)) {
				this.hour = hour;
			} else {
				throw new WrongTimeException("wrong input for hour");
			}
			if (validateMinute(minutes)) {
				this.minutes = minutes;
			} else {
				throw new WrongTimeException("wrong input for minutes");
			}
			if (validateSecond(seconds)) {
				this.seconds = seconds;
			} else {
				throw new WrongTimeException("wrong input for seconds");
			}
		} catch (Exception e) {
			System.out.println(e);
		}
	}

	// -------------------------------------------------------- validations
	public static boolean validateHour(int hr) { // ---------- hour validation
		if (hr > 12 || hr < 0) {
			return false;
		}
		return true;
	}

	public static boolean validateSecond(int sec) { // ---------- validation second
		if (sec > 59 || sec < 0) {
			return false;
		}
		return true;
	}

	public static boolean validateMinute(int min) { // ---------- validation minutes
		if (min > 59 || min < 0) {
			return false;
		}
		return true;
	}

	public static boolean validateStatus(String status) { // ----------validation am/pm
		if (status.matches("(?i)(am|pm)")) {
			return true;
		}
		return false;
	}

	public boolean setStatus(String status) throws WrongArgumentException{ /// set ********************* am/pm

		if (validateStatus(status)) {
			this.status = status;
			return true;
		} else {
			System.out.println("wrong input");
			return false;
		}

	}

	public boolean setNewTime(int hour, int minutes, int seconds) throws WrongTimeException  {
		
			if (validateHour(hour)) {
				this.hour = hour;
			} else {
				throw new WrongTimeException("wrong input for hour");
			}
			if (validateMinute(minutes)) {
				this.minutes = minutes;
			} else {
				throw new WrongTimeException("wrong input for minutes");
			}
			if (validateSecond(seconds)) {
				this.seconds = seconds;
			} else {
				throw new WrongTimeException("wrong input for seconds");
			}
			
			return true;
		 
			
		
	}
}
