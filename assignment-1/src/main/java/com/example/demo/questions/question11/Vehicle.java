package com.example.demo.questions.question11;

//Write unit test for a program which demonstrated:

//On a single track, two vehicles are running. As vehicles are going in same direction there is no problem. If 
//the vehicles are running in opposition direction there is a chance of collision. To avoid collisions write 
//a java program using exception handling. You are free to make necessary assumptions

public class Vehicle {
	private double speed; // km/hr
	private int time; // hr
	private double distance; // km
	private String direction; // right or left
	private int route; // assume route=1 , subRoute= 2

	public String getDirection() {
		return direction;
	}

	public void setDirection(String direction) {
		this.direction = direction;
	}

	public int getRoute() {
		return route;
	}

	public void setRoute(int route) {
		this.route = route;
	}

	public Vehicle(double speed, int time, double distance, String direction, int route) {
		super();
		this.speed = speed;
		this.time = time;
		this.distance = distance;
		this.direction = direction;
		this.route = route;
	}

	public static boolean checkSafety(Vehicle v1, Vehicle v2) {
		boolean flag;
		try {

			if (v1.getDirection().equals(v2.getDirection())) {
				System.out.println("As vehicles are going in same direction there is no problem");
				flag = true;

			} else { // if direction of trains are opposite and on the same route
				flag = false;
				throw new SameDirectionException(
						"As the trains are running in opposition direction towards each other on same route, the dispatcher will route one train into the siding, while the other train passes on the mainline");
			}

		} catch (SameDirectionException e) {
			System.out.println(e.getMessage());

			v1.setRoute(2); // routing train no. 1 to side route
			flag = true;
			if (v1.getRoute() == v1.getRoute()) {
				System.out.println(
						"the trains are running in opposition direction towards each other and the routing has been done succeefully, now trains can passes without collision");
			}

		} catch (Exception e) {
			System.out.println(e);
			flag = false;
		}
		return flag;
	}

	public static void main(String[] args) {

		Vehicle v1 = new Vehicle(80, 1, 1, "rights", 1);
		Vehicle v2 = new Vehicle(80, 1, 1, "left", 1);
		checkSafety(v1, v2);
	}

}
