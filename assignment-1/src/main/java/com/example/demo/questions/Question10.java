package com.example.demo.questions;

import java.util.ArrayList;

//Write unit test for a program to find the duplicate characters in a string

public class Question10 {

	public ArrayList<Character> duplicateChar(String str) {

		ArrayList<Character> list = new ArrayList<>();
		int count = 1;
		char[] ch = str.toCharArray();

		for (int i = 0; i < str.length(); i++) {
			for (int j = i + 1; j < str.length(); j++) {
				if (ch[i] == ch[j]) {
					ch[j] = '*';
					count++;
				}
			}
			if (ch[i] != '*' && count > 1) {
				list.add(ch[i]);
			}
			count = 1;
		}
		return list;
	}

	public static void main(String[] args) {
		String str = "damitam";
		Question10 question10 = new Question10();
		System.out.println(question10.duplicateChar(str));
	}
}
