package com.example.demo.questions.question18;

import org.apache.catalina.User;

/**
 * Build an explicit-value constructor that implements the following algorithm.
 * 
 * Assumptions: myX, myY and myDiameter have been declared as instance variables
 * of type double.
 * 
 * Algorithm (explicit-value constructor) Receive double values for x, y and
 * diameter. If the value of diameter satisfies the invariant, then Store the
 * coordinates and the diameter in the private instance variables. Otherwise:
 * Throw a new exception indicating that the explicit values are invalid. This
 * final step, the throwing of an exception is a departure from what you have
 * done in the past to signal errors, which was always to print an error message
 * and terminate execution of the program. The compiler will require that you do
 * two things the properly handle this exception:
 * 
 * Add a throws exception clause to the declaration of the constructor (after
 * the parameter list, before the opening brace, { ). Modify your console
 * application to use the try-catch construct as follows: try {
 * 
 * your console-based commands (which might throw errors)...
 * 
 * } catch (Exception e) {
 * 
 * print the message encapsulated in the thrown exception...
 * 
 * } You can get a thrown exception’s message using the method
 * exception.getMessage() . Try adding a call to the explicit-value constructor
 * that violates the invariant and check to see that the appropriate error
 * message composed in the circle class is printed appropriately.
 **/
// r2 = x2 + y2

public class Circle {
	private double myX;
	private double myY;
	private double myDiameter;

	public double getMyDiameter() {
		return myDiameter;
	}

	@Override
	public String toString() {
		return "Circle [myX=" + myX + ", myY=" + myY + ", myDiameter=" + myDiameter + "]";
	}

	public Circle(double myX, double myY, double myDiameter) throws InvalidExplicitValues {
		super();

		double r = Math.sqrt(Math.pow(myX, 2) + Math.pow(myY, 2)); //radius
		double diameter = r * 2;
		System.out.println(diameter+"  diameter");

		try {
			if (myDiameter > diameter) {
				throw new InvalidExplicitValues("provided value of diameter " + myDiameter
						+ " is greater than value of diameter calculated is " + diameter);
			} else if (myDiameter < diameter) {
				throw new InvalidExplicitValues("provided value of diameter " + myDiameter
						+ " is less than value of diameter calculated is " + diameter);
			} else {
				this.myX = myX;
				this.myY = myY;
				this.myDiameter = myDiameter;
			}
		} catch (Exception e) {
			System.out.println(e);
		}
	}

	public static Circle getInstance(double myX, double myY, double myDiameter) {

		Circle c = new Circle(myX, myY, myDiameter);
		if (c.getMyDiameter() != 0) {
			return c;
		}
		return null;

	}

	public static void main(String[] args) {
		Circle c = new Circle(3, 4, 6);
		if (c != null) {
			System.out.println(c);
		}

	}
}
