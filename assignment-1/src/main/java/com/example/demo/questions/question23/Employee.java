package com.example.demo.questions.question23;

public abstract class Employee { // Employee
	public abstract double salary();

	public abstract String display();
}
