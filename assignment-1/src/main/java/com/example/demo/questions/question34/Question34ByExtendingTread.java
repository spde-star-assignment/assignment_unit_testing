package com.example.demo.questions.question34;

/**
 * Write unit tests for a program that creates threads by extending Thread class
 * .First thread display “Good Morning “every 1 second, the second thread
 * displays “Hello “every 2 seconds and the third display “Welcome” every 3
 * seconds , (Repeat the same by implementing Runnable)
 **/

class One extends Thread {
	public void run() {
		Question34ByExtendingTread.flag1 = true;
		for (int i = 0; i < 10; i++) {
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				System.out.println(e);
			}
			System.out.println("Good Morning");
		}
	}
}

class Two extends Thread {
	public void run() {
		Question34ByExtendingTread.flag2 = true;
		for (int i = 0; i < 10; i++) {
			try {
				Thread.sleep(2000);
			} catch (InterruptedException e) {
				System.out.println(e);
			}
			System.out.println("Hello ");
		}
	}
}

class Three extends Thread {
	public void run() {
		for (int i = 0; i < 10; i++) {
			try {
				Thread.sleep(3000);
			} catch (InterruptedException e) {
				System.out.println(e);
			}
			System.out.println("Welcome");
		}
	}
}

public class Question34ByExtendingTread {
	public static boolean flag1 = false;
	public static boolean flag2 = false;
	
	public static void main(String[] args) throws InterruptedException {
		One t1 = new One();
		Two t2 = new Two();
		Three t3 = new Three();

		t1.start();
		t2.start();
		t3.start();
		Thread.currentThread().sleep(100);
	}
}
