package com.example.demo.questions;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

//Write unit tests for a program to print all the prime numbers in an array of n elements by taking 
//command line arguments.

public class Question16 {
	public static List<Integer> list = new ArrayList<>();
	
	public static List<Integer> getPrimeList(int[] arr) {
//		List<Integer> list = new ArrayList<>();
		
		for (int i = 0; i < arr.length; i++) {
			boolean flag = true;

			for (int j = 2; j < arr[i]/2+1; j++) {
				if (arr[i] % j == 0) {
					flag = false;
				}
			}
			if (flag && arr[i]!=1) {
				list.add(arr[i]);
			}
		}
		return list;
	}

	public static void main(String[] args) {



		int arr[] = new int[args.length];

		for (int i = 0; i < args.length; i++) {
			arr[i] = Integer.valueOf(args[i]);
		}

		System.out.println((getPrimeList(arr)));
	}
}
