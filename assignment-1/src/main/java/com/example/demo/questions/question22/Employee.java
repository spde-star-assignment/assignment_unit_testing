package com.example.demo.questions.question22;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import org.springframework.boot.SpringApplication;

//Write unit tests for prepare a Employee object containing id, name & department. Sort the employee 

//based on name. Implement the same using Lambda expression

public class Employee {

	private int id;
	private String name;
	private String department;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Employee(int id, String name, String department) {
		super();
		this.id = id;
		this.name = name;
		this.department = department;
	}

	public static ArrayList<Employee> sortEmployee(ArrayList<Employee> empList) {
		Collections.sort(empList, (a, b) -> a.getName().compareTo(b.getName()));
		;
		return empList;
	}

	@Override
	public String toString() {
		return "Employee [id=" + id + ", name=" + name + ", department=" + department + "]";
	}

	@Override
	public boolean equals(Object obj) {

		return ((Integer) ((Employee) (obj)).id).equals(this.id);
	}

	public static void main(String[] args) {
		ArrayList<Employee> empList = new ArrayList<>();

		empList.add(new Employee(1, "amit", "spde"));
		empList.add(new Employee(2, "saurabh", "spde"));
		empList.add(new Employee(3, "adesh", "spde"));
		empList.add(new Employee(4, "vaibhav", "spde"));

		System.out.println(sortEmployee(empList));
	}

}
