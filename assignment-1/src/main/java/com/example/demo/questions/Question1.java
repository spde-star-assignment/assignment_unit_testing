package com.example.demo.questions;


//Write unit tests for a program to show that the value of non-static variable is not visible to all the instances and therefore cannot be used to count the number of instances
public class Question1 {

	 private int countNonStatic=0;  
	 private static int countStatic; 
	 
	 
	 static {
		 countStatic=0;
	 }
	 
	 public Question1() {
		 countNonStatic++;
		 countStatic++;
	 }
	 
	 public int getCountNonStatic() {
		return countNonStatic;
	}
	 
	
	public static int getCountStatic() {
		return countStatic;
	}

	public static void main(String args[]){  
	
		 
	    }  
	
}

 
