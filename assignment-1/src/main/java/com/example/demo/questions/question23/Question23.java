package com.example.demo.questions.question23;

//Write unit tests for:

//Create an abstract class employee, having its properties & abstract function for calculating net salary and 
//displaying the information. Drive manager & clerk class from this abstract class & implement the 
//abstract method net salary and override the display method

public class Question23 { // Question23

	public static void main(String[] args) {

		Worker w = new Worker(1, "amit", 20000, 5000, "abc");
		System.out.println(w.salary());
		w.display();

		Manager m = new Manager(1, "akash", 50000, 10000, "cba");
		System.out.println(m.salary());
		m.display();
	}
}
