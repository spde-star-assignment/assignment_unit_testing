package com.example.demo.questions.question17;

import java.util.Scanner;

//Write unit tests for:
//Define a “Clock” class that does the following: -
//a. Accept hours, minutes and seconds.
//b. Check the validity numbers.
//c. Set the time to AM/PM mode.
//Use necessary constructors and methods to do the above task


public class Question17 { // question class
	public static void main(String[] args) {
		try (Scanner sc = new Scanner(System.in)) {
			System.out.println("Enter hour : ");
			int hr = sc.nextInt();
			System.out.println("Enter minutes : ");
			int min = sc.nextInt();
			System.out.println("Enter seconds : ");
			int sec = sc.nextInt();

			MyClock clock = new MyClock(hr, min, sec);
			System.out.println(clock);

			System.out.println(clock.setStatus("am"));
			System.out.println(clock);
		} catch (Exception e) {
			System.out.println(e);
		}

	}

}
