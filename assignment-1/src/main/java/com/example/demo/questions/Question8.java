package com.example.demo.questions;
import java.util.List;
import java.util.stream.Collectors;

//Write unit tests for a program to create a array list of integer having some values. List those and get 
//the even numbers using stream API.

public class Question8 {

	public List<Integer> getEvenList(List<Integer> list){
		
		return list.stream().filter(a->a%2==0).collect(Collectors.toList());
		
	}  
	
	public static void main(String[] args) {


	}
}
