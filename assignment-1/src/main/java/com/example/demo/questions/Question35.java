package com.example.demo.questions;

/**
 * Write unit tests for a program to print even and odd numbers series
 * respectively from two threads:
 * 
 * t1 and t2 synchronizing on a shared object Let t1 print message “ping — >”
 * and t2 print message “,—-pong”. Take as command line arguments, the following
 * inputs to the program: Sleep Interval for thread t1 Sleep Interval for thread
 * t2 Message per cycle No of cycles
 */
class ThreadOne extends Thread { // remaining
	public void run(int sleepInterval) {
		for (int i = 1; i <= 50; i++) {
			try {
				Thread.sleep(sleepInterval);
			} catch (InterruptedException e) {
				System.out.println(e);
			}
			if (i % 2 == 0) {
				System.out.println(i);
			}

		}
	}
}

class ThreadTwo extends Thread {
	public void run(int sleepInterval) {
		for (int i = 1; i <= 50; i++) {
			try {
				Thread.sleep(sleepInterval);
			} catch (InterruptedException e) {
				System.out.println(e);
			}
			if (i % 2 != 0) {
				System.out.println(i);
			}
		}
	}
}

public class Question35 {

}
