package com.example.demo.questions;

//Write unit tests for a program to count numbers of words in command line argument

public class Question32 {

	public static void main(String[] args) {
		int count = 0;
		for (String string : args) {
			count++;
		}
		System.out.println(count);
//		System.out.println(args.length);
	}

}
