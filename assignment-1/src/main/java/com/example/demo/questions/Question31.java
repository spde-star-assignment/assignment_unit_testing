package com.example.demo.questions;

//Write unit tests for Using methods charAt() & length() of String class, write a program to print the 

//frequency of each character in a string.
//“Hello Java PAT”
//Output should be
//h: 1
//a: 3
//l: 2 (continued for all character in the string

public class Question31 {

	public static void getFrequency(String str) {
		int count = 1;
		StringBuffer str2 = new StringBuffer(str);

		for (int i = 0; i < str.length(); i++) {

			for (int j = i + 1; j < str2.length(); j++) {

				if (str2.charAt(i) == str2.charAt(j)) {
					str2.setCharAt(j, '0');
					count++;
				}
			}
			if (str2.charAt(i) != '0') {
				System.out.print(str.charAt(i) + ": " + count + ", ");
			}
			count = 1;
		}
	}

	public static void main(String[] args) {
		getFrequency("amitam");

	}

}
