package com.example.demo.questions;

import java.time.LocalDate;
import java.time.Period;

//Write unit tests for Prepare a method to accept 2 dates as parameter and return the difference 
//between those 2 dates

public class Question29 {

	public static int dateDiff(LocalDate d1, LocalDate d2) {
		Period period = Period.between(d1, d2);
		return period.getDays();
	}

	public static void main(String[] args) {

		System.out.println(dateDiff(LocalDate.of(22, 12, 21), LocalDate.of(22, 12, 26)));

	}

}
