package com.example.demo.questions.question11;

public class SameDirectionException extends RuntimeException { 
	public SameDirectionException(String msg) {
		super(msg);
	}
}
