package com.example.demo.questions;

import java.io.*;

/**
 * Write unit tests for Write a program that will count the number of lines in
 * each file that is specified on the command line. Assume that the files are
 * text files. Note that multiple files can be specified, as in:java LineCounts
 * file1.txt file2.txt file3.txt Write each file name, along with the number of
 * lines in that file, to standard output. If an error occurs while trying to
 * read from one of the files, you should print an error message for that file,
 * but you should still process all the remaining files. Recommended to use
 * a BufferedReader to process each file.
 * 
 * 
 */
public class Question33 {

	@SuppressWarnings("resource")
	private static void countLines(String fileName) {

		BufferedReader in; // A stream for reading from the file.
		int lineCount; // Number of lines in the file.

		try {
			in = new BufferedReader(new FileReader(fileName));
		} catch (Exception e) {
			System.out.println("error in accessing file");
			return;
		}

		lineCount = 0;

		try {
			String line = in.readLine(); // Read the first line.
			while (line != null) {
				lineCount++; // Count this line.
				line = in.readLine(); // Read the next line.
			}
		} catch (Exception e) {
			System.out.println("problem with reading from file.");
			return;
		}

		System.out.println(lineCount + " lines");

	}

	public static void main(String[] args) {// ******************************************

		if (args.length == 0) {// if no input provided then return
			System.out.println("no input provided");
			return;
		}

		for (int i = 0; i < args.length; i++) {
			String str[] = args[i].split("[\\\\]"); // by splitting get name of file
			int a = str.length - 1;
			System.out.print(str[a] + " :  ");
			countLines(args[i]);
		}

	}
}