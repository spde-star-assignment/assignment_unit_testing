package com.example.demo.questions;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

//Write unit tests for Create an ArrayList of integer having some values. List those and get the even 
//numbers using stream API

public class Question30 {

	public static List<Integer> getEven(List<Integer> list) {
		List<Integer> list2 = list.stream().filter(a -> a % 2 == 0).collect(Collectors.toList());
		return list2;
	}

	public static void main(String[] args) {

		List<Integer> list = new ArrayList<>();
		for (int i = 1; i < 10; i++) {
			list.add(i);
		}
		
		
		System.out.println(getEven(list));

	}
}
