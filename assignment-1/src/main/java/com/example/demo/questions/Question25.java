package com.example.demo.questions;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.zip.DeflaterOutputStream;

/**
 * The "DeflaterOutputStream" and "InflaterInputStream" classes provide
 * mechanism to compress and decompress the data in the deflate compression
 * format.
 * 
 * "DeflaterOutputStream" class The DeflaterOutputStream class is used to
 * compress the data in the deflate compression format. It provides facility to
 * the other compression filters, such as GZIPOutputStream.
 * 
 * The "InflaterInputStream" class is used to decompress the file in the deflate
 * compression format. It provides facility to the other decompression filters,
 * such as GZIPInputStream class.
 * 
 **/

public class Question25 {

	public static boolean compressFile(String str1, String str2) {

		try {
			FileInputStream fileInput = new FileInputStream(str1);
			FileOutputStream fileOutput = new FileOutputStream(str2);
			DeflaterOutputStream dos = new DeflaterOutputStream(fileOutput);

			int i;
			while ((i = fileInput.read()) != -1) {
				dos.write((byte) i);
				dos.flush();
			}
			fileInput.close();
			dos.close();
			return true;
		} catch (Exception e) {
			System.out.println(e);
			return false;
		}
	}

	public static void main(String args[]) {

		String str1 = "C:\\Users\\aanwade\\Desktop\\assignment\\question25\\amit.txt";
		String str2 = "C:\\Users\\aanwade\\Desktop\\assignment\\question25\\amitOutput.txt";

		System.out.println(compressFile(str1, str2));

	}
}