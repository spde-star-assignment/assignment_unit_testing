package com.example.demo.questions.question23;

public class Manager extends Employee {

	private int empId;
	private String name;
	private double salary;
	private double additionalPay;
	private String address;

	
	
	
	public int getEmpId() {
		return empId;
	}


	public String getName() {
		return name;
	}


	public double getSalary() {
		return salary;
	}


	public double getAdditionalPay() {
		return additionalPay;
	}


	public String getAddress() {
		return address;
	}


	public Manager(int empId, String name, double salary, double additionalPay, String address) {
		super();
		this.empId = empId;
		this.name = name;
		this.salary = salary;
		this.additionalPay = additionalPay;
		this.address = address;
	}
	

	@Override
	public double salary() {
		return this.salary + this.additionalPay;
	}

	@Override
	public String display() {
		String str = "Manager [empId=" + empId + ", name=" + name + ", salary=" + salary + ", additionalPay="
				+ additionalPay + ", address=" + address + "]";
		System.out.println(str);
		return str;
	}
}
