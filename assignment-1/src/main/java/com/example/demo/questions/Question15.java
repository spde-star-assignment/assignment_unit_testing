package com.example.demo.questions;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

//Write unit tests for Create the employee table in MySql db perform CRUD operations.

public class Question15 {
	public static Connection getConnection() throws SQLException {
		return DriverManager.getConnection("jdbc:mysql://localhost:3306/amit", "root", "root");
	}

	public  boolean insertData(String username, String password, String fullname, String email) {// insert data
		try {
			Connection con = getConnection();

			String sql = "insert into employeetable (username, password, fullname, email) values (?, ?, ?, ?)";
//			 this query can be used for multiple time using prepareStatement

			PreparedStatement statement = con.prepareStatement(sql);
			statement.setString(1, username);
			statement.setString(2, password);
			statement.setString(3, fullname);
			statement.setString(4, email);

			int rowsInserted = statement.executeUpdate();
			if (rowsInserted > 0) {
				return true;
			}
			con.close();
		} catch (Exception e) {
			System.out.println(e);
		}
		return false;
	}

	public  boolean selectData() throws SQLException { // select data
		String sql = "select * from employeetable;";
		Connection con = getConnection();

		Statement statement = con.createStatement();
		ResultSet result = statement.executeQuery(sql);

		int count = 0;

		try {
			while (result.next()) {
				String name = result.getString(2);
				String pass = result.getString(3);
				String fullname = result.getString("fullname");
				String email = result.getString("email");
				String output = "User #%d: %s - %s - %s - %s";
				System.out.println(String.format(output, ++count, name, pass, fullname, email));
			}
			con.close();
		} catch (Exception e) {
			System.out.println(e);
		}
		if (count == 0) {
			return false;
		}
		return true;
	}

	public  boolean updatePassword(String userName, String password) throws SQLException { //update Password
		Connection con = getConnection();

		try {
			String sql = "update employeetable set password=? where username = ?;";

			PreparedStatement statement = con.prepareStatement(sql);
			statement.setString(1, password);
			statement.setString(2, userName);

			int rowsUpdated = statement.executeUpdate();
			con.close();
			if (rowsUpdated > 0) {
				return true;
			}

		} catch (Exception e) {
			System.out.println(e);
		}
		return false;
	}

	public  boolean deleteEmployee(String userName) throws SQLException { //delete Employee
		Connection con = getConnection();
		try {
			String sql = "delete from employeetable where username=?";

			PreparedStatement statement = con.prepareStatement(sql);
			statement.setString(1, userName);

			int rowsDeleted = statement.executeUpdate();

			con.close();
			if (rowsDeleted > 0) {
				return true;
			}

		} catch (Exception e) {
			System.out.println(e);
		}
		return false;
	}

	public static void main(String[] args) throws SQLException {
		Question15 question15 = new Question15();
//		System.out.println(question15.insertData("aanwade", "password", "Amit Anawde", "amit@gmail.com"));
//		System.out.println(question15.selectData());
//		System.out.println(question15.updatePassword("aanwade","myPass"));
//		System.out.println(question15.deleteEmployee("aanwade"));

	}

}
