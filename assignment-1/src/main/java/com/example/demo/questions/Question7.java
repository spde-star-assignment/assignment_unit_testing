package com.example.demo.questions;

import java.time.*; //Joda time API java 8 feature

//Write unit tests for a program to prepare a method to accept 2 dates as parameter and return the 
//difference between those 2 dates.


public class Question7 {

	public int getDateDiff(LocalDate date1,LocalDate date2) {
		return   Math.abs((Period.between(date1, date2)).getDays());
	}
	
	public static void main(String[] args) {
	
	}
}
