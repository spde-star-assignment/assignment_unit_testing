package com.example.demo.questions.question18;

@SuppressWarnings("serial")
public class InvalidExplicitValues extends RuntimeException {
	public InvalidExplicitValues(String msg) {
		super(msg);
	}
}
