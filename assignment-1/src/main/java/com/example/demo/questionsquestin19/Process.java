package com.example.demo.questionsquestin19;

import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

//Write unit tests for a program to implement the concept of different job scheduling techniques like
//1. First Come First Served [FCFS]
//2. Shortest Job First [SJF]
//3. Round Robin
//4. Priority Scheduling

//Turnaround Time = Burst Time + Waiting Time 

public class Process implements Comparable<Process> {
	private int waitingTime;
	private int burstTime;
	private int arrivalTime;
	private int priority;
	private int rrTime; // time for round robin

	public int getPriority() {
		return priority;
	}


	public int getRrTime() {
		return rrTime;
	}

	public void setRrTime(int rrTime) {
		this.rrTime = rrTime;
	}



	public void setWaitingTime(int waitingTime) {
		this.waitingTime = waitingTime;
	}

	public int getBurstTime() {
		return burstTime;
	}


	public Process(int burstTime, int arrivalTime, int priority) { // for Priority Scheduling
		super();
		this.burstTime = burstTime;
		this.arrivalTime = arrivalTime;
		this.priority = priority;
	}

//	public static Process getProcess(int burstTime, int arrivalTime) { // get Process
//		Process process = new Process(burstTime, arrivalTime);
//		process.setRrTime(burstTime);
//		return process;
//	}

	@Override
	public int compareTo(Process o) { // Override compareTo method
		return ((Integer) (this.getBurstTime())).compareTo(o.burstTime);
	}

//	@Override
//	public String toString() {
//		return "burstTime=" + burstTime + ", arrivalTime=" + arrivalTime + ", waitingTime=" + waitingTime + "\n";
//	}

	public static void calculateWaitingTime(List<Process> sjfList) { // calculate Waiting Time
		int waitingtime = 0;

		for (Iterator<Process> itr = sjfList.iterator(); itr.hasNext();) {
			Process process = (Process) itr.next();
			process.setWaitingTime(waitingtime);
			waitingtime = waitingtime + process.getBurstTime();
		}

	}

	public static List<Process> shortestJobFirst(List<Process> sjfList) { // shortest Job First
		Collections.sort(sjfList);
		calculateWaitingTime(sjfList);
		return sjfList;
	}

	public static List<Process> firstComeFirstServed(List<Process> fcfsList) { // First Come First Served
		Collections.sort(fcfsList, (a, b) -> {
			return ((Integer) (a.arrivalTime)).compareTo(b.arrivalTime);
		});
		calculateWaitingTime(fcfsList);

		return fcfsList;
	}

	/** 3 =  time quantum (which complete first)
	 * 4  1  -2    if(rrTime >0){value -= 3; } else burstTime =0        where rrTime = burstTime
	 * 6  3   0      
	 * 7  4   1
	 * 3  3   3
	 * 2 -1   0     else   rrTime =0
	 * 
	 * maintain max-lastValue(rrTime) variable indicate if value after iteration is 0 then all done  
	 * 
	 * 
	**/

	public static Set<Process> roundRobin(List<Process> rrList, int timeQuantum) { // round Robin
		int maxLastValue = Integer.MIN_VALUE;
		Set<Process> rrList2 = new LinkedHashSet<>();
		Collections.sort(rrList, (a, b) -> {
			return ((Integer) (a.arrivalTime)).compareTo(b.arrivalTime);
		});

		try {
			for (int i = 0; i < rrList.size(); i++) {
//			int waitingTime =0;
				for (Iterator<Process> itr = rrList.iterator(); itr.hasNext();) {
					Process process = (Process) itr.next();

					int temp = process.getRrTime() - timeQuantum;
					if (temp > 0) {
						process.setRrTime(temp);

					} else {
						temp = 0;
						process.setRrTime(temp);
					}
					if (maxLastValue < temp) {
						maxLastValue = temp;
					}
					if (process.getRrTime() == 0) {
						rrList2.add(process);
					}
//					if (process.getRrTime()!=0) { // write logic for wating time	 // can be done using instance variablle
//					}
				}
				if (maxLastValue == 0) {
					break;
				}
			}
		} catch (Exception e) {
			System.out.println(e);
			return null;
		}

		calculateWaitingTime(rrList);
		return rrList2;
	}

	public static List<Process> priorityScheduling(List<Process> psList) { // Priority Scheduling
		Collections.sort(psList, (a, b) -> {
			return ((Integer) (a.priority)).compareTo(b.priority);
		});
		calculateWaitingTime(psList);
		return psList;
	}

	public static void main(String[] args) {

//		List<Process> sjfList = new ArrayList<>();
//
//		Process process1 = new Process(4, 1, 5); // taking args burstTime, arrivalTime
//		Process process2 = new Process(6, 3, 8);
//		Process process3 = new Process(7, 2, 3);
//		Process process4 = new Process(3, 4, 4);
//		Process process5 = new Process(2, 4, 9);
//
//		sjfList.add(process1);
//		sjfList.add(process2);
//		sjfList.add(process3);
//		sjfList.add(process4);
//		sjfList.add(process5);

//		System.out.println("shortest Job First");
//		System.out.println(shortestJobFirst(sjfList));
//
//		System.out.println("First Come First Served");
//		System.out.println(firstComeFirstServed(sjfList));

//		System.out.println("First Come First Served");
//		System.out.println(roundRobin(sjfList,3));

//		System.out.println("Priority Scheduling");
//		System.out.println(priorityScheduling(sjfList));

	}

}
