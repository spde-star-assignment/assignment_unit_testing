package com.example.demo;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import com.example.demo.questions.Question21;

class Question21Test {

	@Test
	void testAddition() {
		int a = 2;
		int b = 4;
		assertEquals(a + b, Question21.add.apply(a, b));
	}

}
