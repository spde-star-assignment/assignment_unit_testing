package com.example.demo;

import static org.junit.Assert.assertTrue;

import java.sql.SQLException;

import org.junit.jupiter.api.Test;

import com.example.demo.questions.Question15;

class Question15Test {

//	System.out.println(insertData("aanwade", "password", "Amit Anawde", "amit@gmail.com"));
//	System.out.println(selectData());
//	System.out.println(updatePassword("aanwade","myPass"));
//	System.out.println(deleteEmployee("aanwade"));

	Question15 question15 = new Question15();

	@Test
	void testInsertData() {
		assertTrue(question15.insertData("aanwade", "password", "Amit Anawde", "amit@gmail.com"));
	}

	@Test
	void testDeleteData() throws SQLException {
		question15.insertData("aanwade", "password", "Amit Anawde", "amit@gmail.com");
		assertTrue(question15.deleteEmployee("aanwade"));
	}

	@Test
	void testSelectata() throws SQLException {
		question15.insertData("aanwade", "password", "Amit Anawde", "amit@gmail.com");
		assertTrue(question15.selectData());
	}

	@Test
	void testUpdatePassword() throws SQLException {
		question15.insertData("aanwade", "password", "Amit Anawde", "amit@gmail.com");
		assertTrue(question15.updatePassword("aanwade", "mypass"));
	}
}
