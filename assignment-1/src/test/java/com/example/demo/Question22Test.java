package com.example.demo;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;

import org.junit.jupiter.api.Test;

import com.example.demo.questions.question22.Employee;

class Question22Test {

	@Test
	void testEmployeeList() {

		ArrayList<Employee> empList1 = new ArrayList<>();
		ArrayList<Employee> empList2 = new ArrayList<>();

		empList1.add(new Employee(1, "amit", "spde")); // list 1
		empList1.add(new Employee(2, "saurabh", "spde"));
		empList1.add(new Employee(3, "adesh", "spde"));
		empList1.add(new Employee(4, "vaibhav", "spde"));

		empList2.add(new Employee(3, "adesh", "spde")); // list 2
		empList2.add(new Employee(1, "amit", "spde"));
		empList2.add(new Employee(2, "saurabh", "spde"));
		empList2.add(new Employee(4, "vaibhav", "spde"));

		assertEquals(empList2, Employee.sortEmployee(empList1));
	}

}
