package com.example.demo;

import static org.junit.Assert.assertArrayEquals;

import java.util.Arrays;

import org.junit.jupiter.api.Test;

import com.example.demo.questions.Question27;
//import com.example.demo.questions.Question27.Sorting;

class Question27Test {

	@Test
	void test() {
		Integer arr[] = { 6, 8, 3, 3, 2, 1, 0 };
		Integer arr1[] = { 6, 8, 3, 3, 2, 1, 0 };
		Arrays.sort(arr1);
		Question27.Sorting<Integer> srtboj = new Question27().new Sorting<Integer>();
		assertArrayEquals(arr1, srtboj.DataSorting(arr));
	}
}
