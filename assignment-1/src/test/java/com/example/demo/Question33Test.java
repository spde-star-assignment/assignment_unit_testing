package com.example.demo;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import org.junit.Assert;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.example.demo.questions.Question31;
import com.example.demo.questions.Question33;

class Question33Test {

	// private final PrintStream standardOut = System.out;
	private final ByteArrayOutputStream outputStreamCaptor = new ByteArrayOutputStream();

	@BeforeEach
	public void setUp() {
		System.setOut(new PrintStream(outputStreamCaptor));
	}

	@Test
	void test() {

		Question33.main(new String[] { "C:\\Users\\aanwade\\Desktop\\assignment\\question33\\time.txt" });
		Assert.assertEquals("time.txt :  14 lines", outputStreamCaptor.toString().trim());
	}
}
