package com.example.demo;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;
import java.util.List;


import org.junit.jupiter.api.Test;

import com.example.demo.questions.Question6;

class Question6Test {

	Question6 question6 = new Question6();

	@Test
	void testCase1() { // test case 1
		int num = 10;
		List<Integer> list = new ArrayList<>();
		for (int i = 2; i <= num; i++) {// adding even numbers to list
			if (i % 2 == 0) {
				list.add(i);
			}
		}
		assertEquals(list, question6.saveEvenNumbers(num));
	}
	
	@Test
	void testCase2() { // test case 2

		int num = 10;
		List<Integer> list = new ArrayList<>();
		for (int i = 2; i <= num; i++) {// adding even numbers to list
			if (i % 2 == 0) {
				list.add(i*2);
			}
		}
		assertEquals(list, question6.printEvenNumbers(10));
	}
	
	@Test
	void testCase3_test1() { // test case 3
		int num = 6;
		assertEquals(6, question6.printEvenNumber(num));	
	}
	
	@Test
	void testCase3_test2() { // test case 3
		int num = 7;
		assertEquals(0, question6.printEvenNumber(num));	
	}

}
