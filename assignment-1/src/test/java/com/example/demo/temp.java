package com.example.demo;

import java.sql.*;

public class temp {

	public static int getCountOfTablesOrViews(String str) {

		int a = 0;
		try {

			Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/imgdb", "root", "root");
			Statement st = con.createStatement();
			ResultSet rs = st
					.executeQuery("SELECT COUNT(*) FROM information_schema."+str+ " WHERE table_schema = 'imgdb'");
			while (rs.next()) {
				a = rs.getInt(1);
			}
			con.close();
		} catch (Exception e) {
			System.out.println(e);
		}
		return a;
	}
	



	public static void main(String args[]) {

		System.out.println(getCountOfTablesOrViews("tables"));
		System.out.println(getCountOfTablesOrViews("views"));
	}
}
