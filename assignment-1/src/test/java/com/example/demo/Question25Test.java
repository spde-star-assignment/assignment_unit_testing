package com.example.demo;

import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.zip.InflaterInputStream;

import org.apache.commons.io.FileUtils;
import org.junit.jupiter.api.Test;

import com.example.demo.questions.Question25;

class Question25Test {

	@Test
	void test() throws IOException {
		
		String str1 = "C:\\Users\\aanwade\\Desktop\\assignment\\question25\\amit.txt";
		String str2 = "C:\\Users\\aanwade\\Desktop\\assignment\\question25\\amitOutput.txt";
		
		Question25.compressFile(str1, str2); // // calling method for compressing
		
		try {// code for decompressing
			FileInputStream fin = new FileInputStream(
					"C:\\Users\\aanwade\\Desktop\\assignment\\question25\\amitOutput.txt");
			InflaterInputStream in = new InflaterInputStream(fin);
			FileOutputStream fout = new FileOutputStream(
					"C:\\Users\\aanwade\\Desktop\\assignment\\question25\\amit2.txt");

			int i;
			while ((i = in.read()) != -1) {
				fout.write((byte) i);
				fout.flush();
			}
			fin.close();
			fout.close();
			in.close();
		} catch (Exception e) {
			System.out.println(e);
		}
		File file1 = new File("C:\\Users\\aanwade\\Desktop\\assignment\\question25\\amit2.txt");

		File file2 = new File("C:\\Users\\aanwade\\Desktop\\assignment\\question25\\amit.txt");

		assertTrue(FileUtils.contentEquals(file1, file2));
	}
}
