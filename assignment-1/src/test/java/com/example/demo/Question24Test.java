package com.example.demo;

import static org.assertj.core.api.Assertions.assertThatExceptionOfType;
//import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertArrayEquals;

import org.junit.jupiter.api.Test;

import com.example.demo.questions.Question24;
import com.mysql.cj.exceptions.WrongArgumentException;

class Question24Test {

	@Test
	void testPrintSquares() {
		int num = 5;
		int arr[] = { 1, 4, 9, 16, 25 };
		assertArrayEquals(arr, Question24.printSqure(num));

	}

	@Test
	void testPrintSquaresException() {
		int num = 11;
		assertThatExceptionOfType(WrongArgumentException.class).isThrownBy(() -> Question24.printSqure(num));
	}
}
