package com.example.demo;

import static org.junit.jupiter.api.Assertions.*;

import java.time.LocalDate;

import org.junit.jupiter.api.Test;

import com.example.demo.questions.Question7;

class Question7Test {

	
	
	@Test
	void testDate() {
		Question7 question7 = new Question7();
		LocalDate date1 = LocalDate.of(2022,9,9);
		LocalDate date2 = LocalDate.of(2022,9,7);
		assertEquals(2, question7.getDateDiff(date1,date2));
	}

}
