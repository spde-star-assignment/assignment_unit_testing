package com.example.demo;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import com.example.demo.questions.question11.Vehicle;

class Question11Test {

	@Test
	void testSameDirection() {
		Vehicle v1 = new Vehicle(80, 1, 1, "left", 1); // speed, distance, time ,direction, route(actual Route=1, sub-Route = 2)
		Vehicle v2 = new Vehicle(80, 1, 1, "left", 1);

		assertTrue(Vehicle.checkSafety(v1, v2)); // as trains in same direction no problem
	}

	@Test
	void testOppositeDirection() {
		Vehicle v1 = new Vehicle(80, 1, 1, "right", 1); // speed, distance, time ,direction, route
		Vehicle v2 = new Vehicle(80, 1, 1, "left", 1);
		assertTrue(Vehicle.checkSafety(v1, v2)); // as trains in opposite direction no problem // returning true assert
													// that exception handling has been done
	}

	@Test
	void testRouting() {
		Vehicle v1 = new Vehicle(80, 1, 1, "right", 1); // speed, distance, time ,direction, route
		Vehicle v2 = new Vehicle(80, 1, 1, "left", 1);
		Vehicle.checkSafety(v1, v2); // as trains in opposite direction no problem // returning true assert
										// that exception handling has been done
		assertEquals(2, v1.getRoute()); // indicate routing has been done successfully

	}
}
