package com.example.demo;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

import org.junit.jupiter.api.Test;

import com.example.demo.questions.Question9;

class Question9Test {

	Question9 question9 = new Question9();
	
	@Test
	void test1() {
		assertEquals(0, question9.getTax(50000)); // no tax upto 50000
	}

	
	@Test
	void test2() {
		assertNotEquals(0, question9.getTax(60000));  // tax on amount above 50000
	}
}
