package com.example.demo;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;

import org.junit.jupiter.api.Test;

import com.example.demo.questions.Question10;

class Question10Test {

	Question10 question10 = new Question10();

	@Test
	void test() {
		String str = "amitam";
		ArrayList<Character> list = new ArrayList<>();
		list.add('a');
		list.add('m');

		assertEquals(list, question10.duplicateChar(str));

	}

}
