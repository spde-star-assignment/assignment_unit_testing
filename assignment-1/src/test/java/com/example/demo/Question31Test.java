package com.example.demo;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import org.junit.Assert;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.example.demo.questions.Question31;

class Question31Test {
//	private final PrintStream standardOut = System.out;
	private final ByteArrayOutputStream outputStreamCaptor = new ByteArrayOutputStream();

	@BeforeEach
	public void setUp() {
		System.setOut(new PrintStream(outputStreamCaptor));
	}

	@Test
	void testetFrequency() {
		Question31.getFrequency("amitam");
		Assert.assertEquals("a: 2, m: 2, i: 1, t: 1,", outputStreamCaptor.toString().trim());
	}
}
