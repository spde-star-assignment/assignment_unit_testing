package com.example.demo;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import org.junit.jupiter.api.Test;

import com.example.demo.questionsquestin19.Process;

class Question19Test {

	
	public static List<Process> getListOfProcess(){
		List<Process> psList = new ArrayList<>();
		Process process1 = new  Process(4, 1,5); // taking args burstTime, arrivalTime, priority
		Process process2 = new  Process(6, 3,8);
		Process process3 = new  Process(7, 2,3);
		Process process4 = new  Process(3, 4,4);
		Process process5 = new  Process(2, 4,9);
		psList.add(process1);
		psList.add(process2);
		psList.add(process3);
		psList.add(process4);
		psList.add(process5);
		return psList;
	}
	
	
	@Test
	void testPriorityScheduling() { // priorityScheduling
		List<Process> psList = getListOfProcess();

		List<Process> psList1 =  Process.priorityScheduling(psList);
		
		Collections.sort(psList, (a, b) -> {
			return ((Integer) (a.getPriority())).compareTo(b.getPriority());
		});
		
	assertEquals(psList,psList1);
	}
	
	@Test
	void testRoundRobinScheduling() { // roundRobin
		List<Process> rrList = getListOfProcess();

		Collections.sort(rrList, (a, b) -> {
			return ((Integer) (a.getPriority())).compareTo(b.getPriority());
		});
		
	assertNotNull( Process.roundRobin(rrList,3));
	}
	
	@Test
	void testShortestJobFirst() {// shortestJobFirst
		List<Process> sjbList = getListOfProcess();
		List<Process> sjbList1 =	Process.shortestJobFirst(sjbList);
		Collections.sort(sjbList, (a, b) -> {
			return ((Integer) (a.getBurstTime())).compareTo(b.getBurstTime());
		});
	assertEquals( sjbList,sjbList1);
	}
	
	@Test
	void testFirstComeFirstServed() { // firstComeFirstServed
		List<Process> fcfsList = getListOfProcess();
		List<Process> fcfsList1 =	Process.firstComeFirstServed(fcfsList);
		Collections.sort(fcfsList, (a, b) -> {
			return ((Integer) (a.getBurstTime())).compareTo(b.getBurstTime());
		});
	assertEquals( fcfsList,fcfsList1);
	}

}
