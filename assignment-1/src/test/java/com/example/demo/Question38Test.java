package com.example.demo;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Period;
import java.time.format.DateTimeFormatter;

import org.junit.jupiter.api.Test;

import com.example.demo.questions.Question38;

class Question38Test {

	@Test
	void test() {

		int hourDiff = 10;

		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MM/dd/yyyy - HH:mm:ss z");
		String date1 = Question38.getDateWithTimeZone();
		String date2 = Question38.AddTimeToExistingDate(hourDiff);

		LocalDateTime localDate1 = LocalDateTime.parse(date1, formatter);
		LocalDateTime localDate2 = LocalDateTime.parse(date2, formatter);
		
		int result = Math.abs(localDate1.getHour() - localDate2.getHour());
		assertEquals(hourDiff, result);

	}

}
