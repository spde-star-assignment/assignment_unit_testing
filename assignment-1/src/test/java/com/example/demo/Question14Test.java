package com.example.demo;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;

import org.junit.jupiter.api.Test;

import com.example.demo.questions.Question14;

class Question14Test {

	Question14 question14 = new Question14();

	@Test
	void testNoOfTable() {
		assertEquals(9, question14.getCountOfTablesOrViews("tables"));
	}

	@Test
	void testNoOfViews() {
		assertEquals(2, question14.getCountOfTablesOrViews("views"));
	}
}
