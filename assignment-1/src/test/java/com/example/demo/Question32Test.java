package com.example.demo;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import org.junit.Assert;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.example.demo.questions.Question32;

class Question32Test {

//	private final PrintStream standardOut = System.out;
	private final ByteArrayOutputStream outputStreamCaptor = new ByteArrayOutputStream();

	@BeforeEach
	public void setUp() {
		System.setOut(new PrintStream(outputStreamCaptor));
	}

	@Test
	void test() {

		Question32.main(new String[] { "amit", "amol", "ajinkya", "abhay" });
		Assert.assertEquals("4", outputStreamCaptor.toString().trim());
	}

}
