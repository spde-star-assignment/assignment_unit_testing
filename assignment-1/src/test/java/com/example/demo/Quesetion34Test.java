package com.example.demo;

import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.example.demo.questions.Question37;
import com.example.demo.questions.question34.Question34ByExtendingTread;

class Quesetion34Test {

	@BeforeEach
	void setUp() throws Exception {
	}

	@Test
	void test1() throws InterruptedException {

		Question34ByExtendingTread.main(new String[] {});

		assertTrue(Question34ByExtendingTread.flag1);

	}

	@Test
	void test2() throws InterruptedException {

		Question34ByExtendingTread.main(new String[] {});

		assertTrue(Question34ByExtendingTread.flag2);

	}
}
