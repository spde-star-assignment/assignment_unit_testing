package com.example.demo;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

import org.junit.jupiter.api.Test;

import com.example.demo.questions.Question1;


class Question1Test {

	@Test
	void nonStaticTest() {
		 Question1 q1 = new Question1();
		 Question1 q2 = new Question1();
		 Question1 q3 = new Question1();
		 assertNotEquals(3, q3.getCountNonStatic());
	}

	
	@Test
	void staticTest() {
		Question1 p1 = new Question1();
		Question1 p2 = new Question1();
		Question1 p3 = new Question1();
		 assertEquals(3, Question1.getCountStatic());
	}
}
