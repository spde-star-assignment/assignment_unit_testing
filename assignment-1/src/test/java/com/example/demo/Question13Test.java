package com.example.demo;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.Assert;
import java.util.Arrays;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.junit.jupiter.api.Test;
import com.example.demo.questions.question13.Employee;
import com.example.demo.questions.question13.EmployeeDB;

class Question13Test {

	EmployeeDB edb = new EmployeeDB();

	@Test
	void testAddEmployee1() {
		Employee e1 = new Employee(2, "amit", "amit@gmail.com", 'm', 50000);
		assertTrue(edb.addEmployee(e1));
	}

	@Test
	void testAddEmployee2() {// if Employee already exist then return false
		Employee e1 = new Employee(2, "amit", "amit@gmail.com", 'm', 50000);
		edb.addEmployee(e1);
		assertFalse(edb.addEmployee(e1)); // adding same emp again
	}

	@Test
	void testDeleteEmployee1() { // delete emp
		Employee e1 = new Employee(1, "amit", "amit@gmail.com", 'm', 50000);
		edb.addEmployee(e1);
		assertTrue(edb.deleteEmployee(1)); // adding same emp again
	}

	@Test
	void testDeleteEmployee2() { // trying to delete emp which does not exist
		Employee e1 = new Employee(1, "amit", "amit@gmail.com", 'm', 50000);
		edb.addEmployee(e1);
		assertFalse(edb.deleteEmployee(2)); // adding same emp again
	}

	@Test
	void testShowPaySlip() {
		Employee e1 = new Employee(1, "amit", "amit@gmail.com", 'm', 50000);
		edb.addEmployee(e1);
		assertEquals("50000.0", edb.showPaySlip(1));
	}

	@Test
	void testListAll() {
		Employee e1 = new Employee(1, "amit", "amit@gmail.com", 'm', 50000);
		Employee e2 = new Employee(2, "amit", "amit@gmail.com", 'm', 50000);
		Employee e3 = new Employee(3, "aj", "amit@gmail.com", 'm', 50000);
		edb.addEmployee(e1);
		edb.addEmployee(e2);
		edb.addEmployee(e3);
		Employee[] emp = new Employee[] { e1, e2, e3 };
		assertArrayEquals(emp, edb.listAll());
	}

}
