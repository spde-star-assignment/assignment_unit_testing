package com.example.demo;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import org.junit.jupiter.api.Test;

import com.example.demo.questions.question18.Circle;

public class Question18Test {

	Circle c =  new Circle(3, 4, 5);

	@Test
	void checkVality1() {// right time
		assertNotNull(Circle.getInstance(3, 4, 10));
	}
	@Test
	void checkVality2() {// right time
		assertNull(Circle.getInstance(3, 4, 6));
	}
}
