package com.example.demo;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

import org.junit.jupiter.api.Test;

import com.example.demo.questions.Question2;

class Question2Test {
	
	Question2 question2 = new Question2();
	
	@Test
	void testDigit() {
		char  ch = '2';
assertEquals("digit", question2.checkCharacter(ch));

	}

	@Test
	void testUpper1() {
char  ch = 'A';
assertEquals("upperCase", question2.checkCharacter(ch));
	}
	@Test
	void testUpper2() {
char  ch = 'A';
assertNotEquals("lowerCase", question2.checkCharacter(ch));
	}
	
	@Test
	void testLower() {
char  ch = 'a';
assertEquals("lowerCase", question2.checkCharacter(ch));
	}
	
	@Test
	void testOther() {
char  ch = '*';
assertEquals("other", question2.checkCharacter(ch));
	}
	
	
}
