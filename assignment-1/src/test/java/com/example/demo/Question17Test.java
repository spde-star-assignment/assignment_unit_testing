package com.example.demo;

import static org.assertj.core.api.Assertions.assertThatExceptionOfType;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertFalse;

import org.junit.jupiter.api.Test;

import com.example.demo.questions.question17.MyClock;
import com.example.demo.questions.question17.Question17;
import com.example.demo.questions.question17.WrongTimeException;

class Question17Test {

	Question17 question17 = new Question17();
	MyClock myClock = new MyClock();

	@Test
	void testSetRightTime() {// right time
		assertTrue(myClock.setNewTime(2, 23, 23));
	}

	@Test
	void testSetWrongTime() {// wrong time
		assertThatExceptionOfType(WrongTimeException.class).isThrownBy(() -> myClock.setNewTime(13, 23, 23));
	}

	@Test
	void testSetStatus1() {// am pm //right
		MyClock myClock = new MyClock(2,34,56);
		assertTrue(myClock.setStatus("am"));
	}

	@Test
	void testSetStatus2() {// am pm //wrong
		assertFalse(myClock.setStatus("ak"));
	}

}
