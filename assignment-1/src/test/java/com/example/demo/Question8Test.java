package com.example.demo;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.junit.jupiter.api.Test;

import com.example.demo.questions.Question8;

//Write unit tests for a program to create a array list of integer having some values. List those and get 
//the even numbers using stream API.

public class Question8Test {

	@Test
	void test() {
		Question8 question8 = new Question8();

		List<Integer> list1 = new ArrayList<>(); // list 1 to 10
		for (int i = 1; i <= 10; i++) {
			list1.add(i);
		}

		List<Integer> list2 = new ArrayList<>(); // list of even numbers
		for (int i = 1; i <= 10; i++) {
			if (i % 2 == 0) {
				list2.add(i);
			}
		}
		
		assertEquals(list2, question8.getEvenList(list1));

	}

}
