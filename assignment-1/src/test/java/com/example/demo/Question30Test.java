package com.example.demo;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;

import com.example.demo.questions.Question30;

class Question30Test {

	@Test
	void testGetEvenMethod() {
		List<Integer> list1 = new ArrayList<>();
		List<Integer> list2 = new ArrayList<>();

		for (int i = 1; i < 10; i++) {
			if (i % 2 == 0) {
				list1.add(i);
			}
			list2.add(i);
		}
		assertEquals(list1, Question30.getEven(list2));
	}
}
