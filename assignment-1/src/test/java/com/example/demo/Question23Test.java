package com.example.demo;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import com.example.demo.questions.question23.Manager;
import com.example.demo.questions.question23.Worker;

class Question23Test {

//	Worker w = new Worker(1, "amit", 20000, 5000, "abc");
//	System.out.println(w.salary());
//	w.display();
//
//	Manager m = new Manager(1, "akash", 50000, 10000, "cba");
//	System.out.println(m.salary());
//	m.display();

	@Test
	void testManagerSalary() {
		Manager m = new Manager(1, "akash", 50000, 10000, "cba");
		assertEquals(60000, m.salary());
	}

	@Test
	void testWorkerSalary() {
		Worker w = new Worker(1, "amit", 20000, 5000, "abc");
		assertEquals(25000, w.salary());
	}

	@Test
	void testManagerDisplay() {
		Manager m = new Manager(1, "akash", 50000, 10000, "cba");
		String str = "Manager [empId=" + m.getEmpId() + ", name=" + m.getName() + ", salary=" + m.getSalary()
				+ ", additionalPay=" + m.getAdditionalPay() + ", address=" + m.getAddress() + "]";
		assertEquals(str, m.display());
	}

	@Test
	void testWorkerDisplay() {
		Worker w = new Worker(1, "amit", 20000, 5000, "abc");

		String str1 = "Worker [workerId=" + w.getWorkerId() + ", name=" + w.getName() + ", salary=" + w.getSalary()
				+ ", additionalPay=" + w.getAdditionalPay() + ", address=" + w.getAddress() + "]";

		assertEquals(str1, w.display());
	}

}
