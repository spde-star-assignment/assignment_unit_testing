package com.example.demo;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.time.LocalDate;
import java.time.Period;

import org.junit.jupiter.api.Test;

import com.example.demo.questions.Question29;

class Question29Test {

	@Test
	void testDateDiff() {
		LocalDate d1 = LocalDate.of(22, 12, 21);
		LocalDate d2 = LocalDate.of(22, 12, 26);
		assertEquals(Period.between(d1, d2).getDays(), Question29.dateDiff(d1,d2));
	}

}
