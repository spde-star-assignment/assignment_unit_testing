import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import com.example.demo.Question8Test;
import com.example.demo.questions.Question6;
import com.example.demo.questions.Question8;

@RunWith(Suite.class)
@SuiteClasses({Question8Test.class})
public class AllTests {

}
