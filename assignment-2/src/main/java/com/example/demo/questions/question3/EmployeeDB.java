package com.example.demo.questions.question3;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.TreeSet;


public class EmployeeDB { // class 1 // **************************************************************
	ArrayList<Employee> empList = new ArrayList<>();
	TreeSet<Employee> empSet = new TreeSet<>();

	public boolean addEmployee(Employee e) { // add Employee
		return (empList.add(e) && empSet.add(e));
	}

	public boolean deleteEmployee(int empId) { // delete Employee
		Iterator<Employee> itr = empList.iterator();

		while (itr.hasNext()) {
			Employee emp = itr.next();
			if (emp.getEmpId() == empId) {
				empList.remove(emp);
				empSet.remove(emp);
				return true;
			}
		}
		return false;
	}

	public String showPaySlip(int empId) { // show pay slip

		Iterator<Employee> itr = empSet.iterator();
		while (itr.hasNext()) {
			Employee emp = itr.next();
			if (emp.getEmpId() == empId) {
				return emp.getEmpSalary()+"";
			}
		}
		return "Employee " + empId + " is not present or wrong employee Id";
	}

	public Employee[] listAll() {
		Employee[] emp = new Employee[empSet.size()];
		int i = 0;
		for (Employee employee : empSet) {
			emp[i] = employee;
			i++;
		}

//		return empList.toArray(new Employee[empSet.size()]);
		return emp;
	}

}

