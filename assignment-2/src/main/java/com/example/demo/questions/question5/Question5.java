package com.example.demo.questions.question5;

import java.io.*;

/**
 * A small company dealing with transportation has just purchased a computer for
 * its new automated reservations system. You have been asked to program the new
 * system. You are to write a program called ReservationSystem to assign seats
 * on a vehicle. Your class also requires the following: a constructor method,
 * which initialise the variables a method to assign the capacity of seating. a
 * method for assigning seats. Use proper data structure to represent the
 * seating chart of the plane. Initialize all the elements of the data structure
 * to first index to indicate that all the seats are empty. As each seat is
 * assigned, set the corresponding elements of the data structure to second
 * index to indicate that the seat is no longer available. Your program should,
 * of course never assign a seat that has already been assigned. appropriate
 * mutator and accessor methods The company also needs a program dealing
 * especially with its only plane with each flight having a capacity of 10
 * seats. Name this class AirlineReservationSystem. This class is a type of
 * ReservationSystem but the way it reserves seats are different. Your program
 * should display the following menu of alternatives for reserving a seat on the
 * flight: Please type 1 for “smoking” Please type 2 for “non-smoking” If the
 * person types 1,then your program should assign a seat in the smoking
 * section(seats 1-5) If the person types 2,then your program should assign a
 * seat in the non-smoking section(seats 6-10). Your program should then print a
 * boarding pass indicating the person’s seat number and whether it is in the
 * smoking or non-smoking section of the plane. When the smoking section is
 * full, your program should ask the person if it is acceptable to be placed in
 * the non-smoking section (and vice versa). If yes, then make the appropriate
 * seat assignment. If no, then print the message “Next flight leaves in 3
 * hours.” Create a main() method to test the next scheduled fligh
 *
 */

class Question5 {

}
