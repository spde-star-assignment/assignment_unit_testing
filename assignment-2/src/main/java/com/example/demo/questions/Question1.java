package com.example.demo.questions;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

//Write unit tests for:

//Develop a java class with a method saveEvenNumbers(int N) using ArrayList to store even numbers from 2 
//to N, where N is a integer which is passed as a parameter to the method saveEvenNumbers(). The 
//method should return the ArrayList (A1) created.

//In the same class create a method printEvenNumbers()which iterates through the arrayList A1 in step 1, 
//and It should multiply each number with 2 and display it in format 4,8,12….2*N. and add these 
//numbers in a new ArrayList (A2). The new ArrayList (A2) created needs to be returned.

//Create a method printEvenNumber(int N) parameter is a number N. This method should search 
//the arrayList (A1) for the existence of the number ‘N’ passed. If exists it should return the Number 
//else return zero.

public class Question1 {

	public ArrayList<Integer> saveEvenNumbers(int n) { // case 1 // return A1 even numbers list
		ArrayList<Integer> list = new ArrayList<>();
		for (int i = 2; i <= n; i++) {
			if (i % 2 == 0) {
				list.add(i);
			}
		}
		return list;
	}

	public List<Integer> printEvenNumbers(int num) { // case 2 // return A2 => A1 * 2 // taking argument for numbers range in case 1
		ArrayList<Integer> list1 = saveEvenNumbers(num); 
		System.out.println(list1);
		return list1.stream().map(a -> a * 2).collect(Collectors.toList());
	}

	public int printEvenNumber(int num) { // case 3 searching num in A1 list
		ArrayList<Integer> list = saveEvenNumbers(2 * num);
		for (Integer integer : list) {
			if (integer == num) {
				return num;
			}
		}
		return 0;
	}

	public static void main(String[] args) {

	}
}
