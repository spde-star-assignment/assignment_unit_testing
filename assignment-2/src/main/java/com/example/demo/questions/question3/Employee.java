package com.example.demo.questions.question3;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.TreeSet;

import org.springframework.stereotype.Component;

//	Write unit tests for:

//Create an application for employee management having following classes:
//1. Create an Employee class with following attributes and behaviours :
//1. EmpId Int
//1. EmpName String
//1. EmpEmail String
//1. EmpGender char
//1. EmpSalary float
//1.GetEmployeeDetails() -> prints employee details

//Create one more class EmployeeDB which has the following methods.
//1.boolean addEmployee(Employee e)
//1.boolean deleteEmployee(int empId)
//1. String showPaySlip(int empId)
//1. Employee[] listAll()

//Do implementation of the above application with below two methods :
//1. Use an ArrayList which will be used to store the employees and use enumeration/iterator to 
//process the employees.
//1. Use a TreeSet Object to store employees on the basis of their EmpId and use 
//enumeration/iterator to process employees.

//Hint: Use Comparable interface] Write a Program to test that all functionalities are operational.

public class Employee implements Comparable<Employee> { // class 2 //
														// **************************************************************

	private int empId;
	private String empName;
	private String empEmail;
	private char empGender;
	private float empSalary;

	public int getEmpId() {
		return empId;
	}

	public void setEmpId(int empId) {
		this.empId = empId;
	}

	public float getEmpSalary() {
		return empSalary;
	}

	public void setEmpSalary(float empSalary) {
		this.empSalary = empSalary;
	}

//**************************************************************
 
	@Override
		public boolean equals(Object obj) {
			return ((Integer)((Employee) obj).empId).equals(this.empId);
		}

	
	
	
	@Override
	public String toString() {
		return "Employee [empId=" + empId + ", empName=" + empName + ", empEmail=" + empEmail + ", empGender="
				+ empGender + ", empSalary=" + empSalary + "]";
	}

	public void GetEmployeeDetails() {
		System.out.println("Employee [empId=" + this.empId + ", empName=" + this.empName + ", empEmail=" + this.empEmail
				+ ", empGender=" + this.empGender + ", empSalary=" + this.empSalary + "]");
	}

	@Override
	public int compareTo(Employee o) {
		return ((Integer) (this.getEmpId())).compareTo((Integer) o.empId);
	}

	public Employee() {
		super();
	}

	public Employee(int empId, String empName, String empEmail, char empGender, float empSalary) {
		super();
		this.empId = empId;
		this.empName = empName;
		this.empEmail = empEmail;
		this.empGender = empGender;
		this.empSalary = empSalary;
	}

	public static void main(String[] args) {// **************************************************************
		EmployeeDB edb = new EmployeeDB();

		// adding employee
		Employee e1 = new Employee(2, "amit", "amit@gmail.com", 'm', 50000);
		Employee e2 = new Employee(3, "amit", "amit@gmail.com", 'm', 50000);
		Employee e3 = new Employee(1, "aj", "amit@gmail.com", 'm', 50000);
		edb.addEmployee(e1);
		edb.addEmployee(e2);
		edb.addEmployee(e3);

		System.out.println(Arrays.toString(edb.listAll()));

		System.out.println(edb.showPaySlip(1));
		System.out.println(edb.deleteEmployee(1));
		System.out.println(edb.showPaySlip(1));

	}
}

//**************************************************************
