package com.example.demo.questions;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;


// using LinkedList
public class Question2 {

	public LinkedList<Integer> saveEvenNumbers(int n) { // case 1 // return A1 even numbers list
		LinkedList<Integer> list = new LinkedList<>();
		for (int i = 2; i <= n; i++) {
			if (i % 2 == 0) {
				list.add(i);
			}
		}
		return list;
	}

	public List<Integer> printEvenNumbers(int num) { // case 2 // return A2 => A1 * 2 // taking argument for numbers range in case 1
		LinkedList<Integer> list1 = saveEvenNumbers(num); 
		System.out.println(list1);
		return list1.stream().map(a -> a * 2).collect(Collectors.toList());
	}

	public int printEvenNumber(int num) { // case 3 searching num in A1 list
		LinkedList<Integer> list = saveEvenNumbers(2 * num);
		for (Integer integer : list) {
			if (integer == num) {
				return num;
			}
		}
		return 0;
	}

	public static void main(String[] args) {

	}
}
