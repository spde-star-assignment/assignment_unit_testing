package com.example.demo.questions;

import java.util.Arrays;

//Write unit tests for a program to sort the any type of data by using inner class without using 
//predefined? 

public class Question7 {

	public class Sorting<T> {

		public <T extends Comparable<? super T>> T[] DataSorting(T[] arr) {

			for (int i = 0; i < arr.length - 1; ++i) {
				int minIndex = i;
				for (int j = i + 1; j < arr.length; ++j) {
					if (arr[j].compareTo(arr[minIndex]) < 0) {
						minIndex = j;
					}
				}
				T temp = arr[i];
				arr[i] = arr[minIndex];
				arr[minIndex] = temp;
			}
			return arr;
		}

	}

	public static void main(String[] args) {
		Integer arr[] = { 6, 8, 3, 3, 2, 1, 0 };

		Sorting<Integer> srtboj = new Question7().new Sorting<Integer>();
		srtboj.DataSorting(arr);
		System.out.println(Arrays.toString(arr));
	}
}
