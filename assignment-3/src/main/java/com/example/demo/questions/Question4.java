package com.example.demo.questions;

//Write unit tests for a Program illustrating Daemon Threads

//Points to remember for Daemon Thread in Java
//	It provides services to user threads for background supporting tasks. 
//It has no role in life than to serve user threads.
//	Its life depends on user threads.
//	It is a low priority thread.

public class Question4 extends Thread {

	public static boolean  flag = false;
	
	public void run() {
		if (Thread.currentThread().isDaemon()) {// checking for daemon thread
			flag = true;
		}
	}

	 static {  //will be executed at the time of class loading
		Question4 t1 = new Question4();// creating thread
		t1.setDaemon(true);// now t1 is daemon thread
		t1.start();// starting threads
	}
	
//	public void createDaemonThread() {  //will be executed at the time of class loading
//			Question4 t1 = new Question4();// creating thread
//			t1.setDaemon(true);// now t1 is daemon thread
//			t1.start();// starting threads
//		}
//	
//	public void createThread() {  //will be executed at the time of class loading
//		Question4 t2 = new Question4();// creating thread
//		t2.start();// starting threads
//	}
//	

	public static void main(String[] args) throws InterruptedException {
		Thread.currentThread().sleep(200);
	System.out.println(flag);
	}

}
